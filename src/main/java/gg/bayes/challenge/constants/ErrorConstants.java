package gg.bayes.challenge.constants;

public final class ErrorConstants {
    //    private ErrorConstants(){}
    public static final String FILE_UPLOAD_SUCCESSFUL = "file-uploaded";
    public static final String FILE_IS_EMPTY = "file is empty";
}
